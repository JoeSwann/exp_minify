
$(document).ready(function (e) {
	
});

function bindFooterTooltip() {
	$('footer .logos').bind('mousemove', function (event) {
		var offset = $(this).offset();
		var offset_left = event.pageX - offset.left + 5;
		var offset_top = event.pageY - offset.top - 40;
		
		$('footer .logos .tooltip').css({
			'left': offset_left,
			'top': offset_top
		});
			
	});
}


function initHashManager() {
	//linkable tabs because this is a multifunction page
	if (window.location.hash) {
		hashChange();
	}
	
	$(window).bind('hashchange', function () {
		if (window.location.hash) {
			hashChange();
		}
	});
}

function hashChange() {
	 var duration    = 1000;
	 var easing      = 'swing';
	var hash = window.location.hash.replace('#', '');

	//contact dropdown handler
	if (hash == 'contact-form') 
		contactDrop();
	else {
		var target = $('h1.banner-heading').offset().top;
		
		if($.browser.safari) 
			var animationSelector='body:not(:animated)';
		else {
			var animationSelector='html:not(:animated)';
		}
		
		$(animationSelector).animate({ scrollTop: target }, duration, easing, function() {});
	}
}

function initContactDropdown() {
	var isFixed = false;

	$contact_drop = $('#contact-drop');
	var contact_drop_height = $contact_drop.innerHeight();

	$('#contact-drop').remove();
	$('body').prepend($contact_drop);

	if (isFixed || isIE8) {
		$contact_drop.css({
			'position': 'fixed',
			'top': '-' + contact_drop_height + 'px',
			'width': '100%'
		});
	}
	else {
		$contact_drop.css({
			'margin-top': '-' + contact_drop_height + 'px'
		});
	}
	
	//contactDropInitMap();

	$('a[href=#contact-form]').click(function (e) {
		if ($.browser.safari) var animationSelector = 'body:not(:animated)';
		else var animationSelector = 'html:not(:animated)';
		$(animationSelector).animate({
			scrollTop: 0
		});
		contactDrop();
		return false;
	});

	$('#contact-drop .close').click(function (e) {
		if (isIE8) {
			$('#contact-drop').animate({
				'top': -contact_drop_height
			}, {
				'duration': 500
			});
		} else {
			$('#contact-drop').animate({
				'margin-top': -contact_drop_height
			}, 500);
		}
		return false;
	});
}

function contactDropInitMap() {
	var map;
	var mapStyles = [{
		featureType: 'all',
		stylers: [{
			saturation: -100
		}]
	}, {
		featureType: 'water',
		stylers: [{
			saturation: -100,
			lightness: -100
		}]
	}]

	var pos = new google.maps.LatLng(-36.82069, 174.747192);
	var myOptions = {
		zoom: 12,
		center: pos,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles: mapStyles
	};

	map = new google.maps.Map(document.getElementById('contact-drop-map'), myOptions);
	var marker = new google.maps.Marker({
		map: map,
		position: pos,
		visible: true
	});
}

function contactDrop() {
	var isFixed = false;
	var contact_drop_height = $contact_drop.innerHeight();
	$contact_drop = $('#contact-drop');
	
	if (isIE8) {
		$('#contact-drop').animate({
			'top': 0
		}, {
			'duration': 500
		});
	} else {
		$('#contact-drop').animate({
			'margin-top': 0
		}, 500);
	}
}


function initHeader() {
        $('header .register').mouseenter(function() {
                $(this).animate({'opacity' : 0.8}, 100);
        }).mouseleave(function() {
                $(this).animate({'opacity' : 1}, 100);
        });
}

function initHomeSlider() {
        $('header, .big-slider').wrapAll('<div class="top-wrap" />');
        $('.top-wrap').append('<div class="loading" />');
}

function initHomeProductHover() {
		$('section.products a img').css({'margin-top' : 10,'margin-bottom' : 0});
	$('section.products a').mouseenter(function() {
		$(this).find('img').fadeTo(100, 0.7);
		$(this).find('img').clearQueue().animate({'margin-top' : 0,'margin-bottom' : 5}, 200);
	}).mouseleave(function() {
		$(this).find('img').clearQueue().fadeTo(100, 1);
		$(this).find('img').clearQueue().animate({'margin-top' : 5,'margin-bottom' : 0}, 200);
	});
}
function initCertLightbox() {
	$('a.see-certificate').bind('click', function() {
		var img_url = $(this).attr('href');
		
		$([img_url]).preload(function(e){
			var lightboxHTML = '<div class="lightbox-close" >X</div><img src="' + img_url + '" />';
			createLightbox(lightboxHTML, 'further-details');
		});
		
		return false;
	});
	
}
function createLightbox(lightbox_content, lightbox_class) {
	$('body').append('<div class="lightbox-wrap" ><div class="lightbox-position"><div class="lightbox ' + lightbox_class + '" /></div></div>');
	
	$lightbox = $('.lightbox');
	$lightbox_position = $('.lightbox-position');
	$lightbox_wrap = $('.lightbox-wrap');
	
	$lightbox_wrap.css({
		position : 'fixed',
		left : 0,
		top : 0,
		width : '100%',
		height : '100%',
		background : '#000',
		background : 'rgba(0,0,0,0.5)',
		'z-index' : 999999
	});
	
	
	$lightbox_position.css({
		position : 'fixed',
		right : 0,
		bottom : 0,
		width : '50%',
		height : '50%'
	});
	
	$lightbox.css({
		position : 'absolute',
		left : '-99999px',
		top : '-99999px',
		width : 'auto',
		height : 'auto',
		padding : '0',
		background : '#fff',
		border : '1px solid #ddd'
	});
	
	$lightbox.append(lightbox_content);
	
	$lightbox_close = $('.lightbox-close');
	$lightbox_close.css({
		position : 'absolute',
		right : '-32px',
		top : '-30px',
		width : '20px',
		height : '20px',
		color : '#fff',
		'font-size' : '20px',
		cursor : 'pointer',
		padding : '0'
	});
	
	var top_offset = -1 * ($lightbox.outerHeight() / 2);
	var left_offset = -1 * ($lightbox.outerWidth() / 2);
	
	$lightbox.css({
		left : left_offset,
		top : top_offset
	});
	
	$('.lightbox-wrap, .lightbox-position, .lightbox-close').live('click', function(e) {
		if(e.target == this){
			$('.lightbox-wrap, .lightbox-position').fadeOut(1000, function() {
				$('.lightbox-wrap, .lightbox-position').remove();
			});
		}
	});
}