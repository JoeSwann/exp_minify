<?php

header('Content-Type: text/html; charset=utf-8');

function getAjax($url = false) {
	if($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}

function sanitizeOutput($buffer)
{
    $search = array(
        '/\>[^\S ]+/s', //strip whitespaces after tags, except space
        '/[^\S ]+\</s', //strip whitespaces before tags, except space
        '/(\s)+/s',  // shorten multiple whitespace sequences
        '/(?!<\")\/\*[^\*]+\*\/(?!\")/'
        );
    $replace = array(
        '>',
        '<',
        '\\1',
        ' '
        );
    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}


$cached = (filemtime('minify/lastcached.txt') < (time() - 3600) 
	&& file_exists('minify/min.js') 
	&& file_exists('minify/min.zip.css')) ? true : false;

if($cached) {
	// JS
	$js = '';
	$js  = getAjax('http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js?ver=3.4.2');
	$js .= getAjax('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js');
	$js .= file_get_contents('js/init.js');
	
	$regex = array(
		"`^([\t\s]+)`ism"=>'',
		"`^\/\*(.+?)\*\/`ism"=>"",
		"`([\n\A;]+)\/\*(.+?)\*\/`ism"=>"$1",
		"`([\n\A;\s]+)//(.+?)[\n\r]`ism"=>"$1\n",
		"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism"=>"\n",
		"/\/\/(.*)[\r\n]/"=>""
	);
	
	$js = preg_replace( '#\s+#', ' ', $js );
    $js = preg_replace( '#/\*.*?\*/#s', '', $js );
    $js = str_replace( '; ', ';', $js );
    $js = str_replace( ': ', ':', $js );
    $js = str_replace( ' {', '{', $js );
    $js = str_replace( '{ ', '{', $js );
    $js = str_replace( ', ', ',', $js );
    $js = str_replace( '} ', '}', $js );
    $js = str_replace( ';}', '}', $js );
	$js = preg_replace(array_keys($regex),$regex,$js);
	
	//deferred loading cant handle gzip (or can it?)
	file_put_contents('minify/min.js', $js);
	
	// CSS
	$css = getAjax('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css');

    $css = preg_replace( '#\s+#', ' ', $css );
    $css = preg_replace( '#/\*.*?\*/#s', '', $css );
    $css = str_replace( '; ', ';', $css );
    $css = str_replace( ': ', ':', $css );
    $css = str_replace( ' {', '{', $css );
    $css = str_replace( '{ ', '{', $css );
    $css = str_replace( ', ', ',', $css );
    $css = str_replace( '} ', '}', $css );
    $css = str_replace( ';}', '}', $css );
	$css = preg_replace(array_keys($regex),$regex,$css);

	$css = gzcompress($css, 9);
	file_put_contents('minify/min.zip.css', $css);
	
	file_put_contents('minify/lastcached.txt', time());
	
	$cached = 'cache generated';
} else {
	$cached = 'cached';
}

$new_css = file_get_contents('minify/min.zip.css');
$new_css = gzuncompress($new_css);

ob_start("sanitizeOutput");

?><html lang="en">	
	<head>
		<style type="text/css"><?php echo $new_css ?>.container{width:960px;margin:40px auto}pre{white-space:pre-wrap;word-break:break-all}</style>
	</head>
	<body>
		<div class="container">
			<pre>
			<?php echo $cached;  ?>
			</pre>
		</div>
	</body>
</html>

<script type="text/javascript">
function downloadJSAtOnload() {
	 var element = document.createElement("script");
	 element.src = "minify/min.js";
	 document.body.appendChild(element);
}

function initGAAtOnload() {
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-18492577-2']);
	_gaq.push(['_trackPageview']);

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
}

 if (window.addEventListener)
 	window.addEventListener("load", downloadJSAtOnload, false);
 else if (window.attachEvent)
 	window.attachEvent("onload", downloadJSAtOnload);
 else {
 	window.onload = downloadJSAtOnload;
 	window.onload = initGAAtOnload;
 }
</script>

<?php ob_end_flush() ?>